<?php
/**
 * Plugin Name: Contador de visitas
 * Description: Pluguin contar el numero de visitas.
 * Version: 1.0
 * Author: Gabriel, Larisa, Cristina
 * License: GPL2
 */
	define( 'ROOT', plugins_url( '', __FILE__ ) );
	define( 'SCRIPTS', ROOT . '/js/' );
	//el archivo upgrade.php contiene la funcion dbDelta
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

function contar_visitas(){
	//es una variable global que contiene el prefijo del nombre de las tablas
	global $wpdb;

	$nomTable = $wpdb->prefix."num_visitas";
	// funcion dbDelta, ejecuta la query que contenga dentro.
	$crea_tabla = dbDelta(
		"CREATE TABLE IF NOT EXISTS $nomTable( id_visitas INT(9) PRIMARY KEY AUTO_INCREMENT NOT NULL, fecha date, numero_visitas mediumint(9))DEFAULT CHARSET=utf8;"
	);

	//obtiene la fecha actual
	$fecha = Date("Y-m-d");
	//datos a introducir por primera vez, inician ambos con 0
	$datos = array(
		'fecha' => $fecha,
		'numero_visitas' => '0'
	);
 	//indica el formato de los datos, % s es para cadena, % d para numeros entero, pero no tiene formato para la fecha(date).
	$formato = array(
		'% s',
		'% d',
		'% d'
	);
	//SELECT numero de visitas FROM $nomTable WHERE id_visitas = 1
	$visita_db = $wpdb->get_results("SELECT * FROM $nomTable");

	$visitas_obj = $wpdb->get_row("SELECT * FROM $nomTable WHERE id_visitas = 1");
	//UPDATE wp_num_visitas SET numero_visitas=numero_visitas+1 WHERE id_visitas=1;

	/*si la consulta devuelve algo se suma 1 al numero de visitas y se hace un update, de lo contrario se hace un insert por primera ves.*/

        $valor_visitas = $visitas_obj->numero_visitas;
        //echo '<script language="javascript">alert("Prueba 1 ' . $valor_visitas . '");</script>';

	if($visita_db){
        
        //echo '<script language="javascript">alert("Prueba 2 ' . $valor_visitas . '");</script>';
        $valor_visitas++;
        //echo '<script language="javascript">alert("Prueba 3 ' . $valor_visitas . '");</script>';
        $datos2 = array('fecha' => $fecha, 'numero_visitas' => $valor_visitas);
        //echo '<script language="javascript">alert("Prueba 4 ' . $valor_visitas . '");</script>';
        $wpdb->update($nomTable, $datos2, array('id_visitas' => 1));
        //echo '<script language="javascript">alert("Prueba 5 ' . $valor_visitas . '");</script>';
    }
    else{
        //echo '<script language="javascript">alert("Prueba 0 ' . $valor_visitas . '");</script>';
        $wpdb->insert($nomTable, $datos);
        //echo '<script language="javascript">alert("Prueba 0.1 ' . $valor_visitas . '");</script>';
    }
    //echo '<script language="javascript">alert("Prueba salida ' . $valor_visitas . '");</script>';


}
	/*se llama cuando el plugin se instala
	funcion llama a un fichero que este dentro del __FILE__(home) ejemplo:
	wp-content/plugins/myplugin.php or
	wp-content/plugins/myplugin/myplugin.php
	y llama a la funcion que se encuentra en este fichero
	*/
	//register_activation_hook( __FILE__, 'contar_visitas' ); 
	//
	//se activa cada vez que se carga la pagina donde este alojada
	add_shortcode('visitas', 'contar_visitas');
	//echo '<script language="javascript">alert("Prueba shortcode ' . $valor_visitas . '");</script>';


function script_muetra_visitas(){
	wp_enqueue_script(
			'upcoming-events',
			SCRIPTS . 'script.js',
			array( 'jquery', 'jquery-ui-datepicker' ),
			'1.0',
			true
	);
}
add_action( 'init', 'script_muetra_visitas' );

//---------------------------------------------------------------
//creando una url para una api simple de nuestra tabla
function api_personalizada(){
	global $wpdb;

	$nomTable = $wpdb->prefix."num_visitas";
	//echo $nomTable.'<br>';
	$vistas_obj = $wpdb->get_row("SELECT * FROM $nomTable WHERE id_visitas = 1");

	//echo $vistas_obj->fecha;
	//echo $vistas_obj->numero_visitas;

	$obj_contador = array(
		'fecha' => $vistas_obj->fecha,
		'numero_visitas' => $vistas_obj->numero_visitas
	);
	/*if($vistas_obj){
		return null;
	}*/
	return $obj_contador;
}

//action que añade y registra url para mostrar lo que la funcion api_personalisada() retorna
add_action('rest_api_init',function (){
	//contador es la ruta sobre la ruta principal a la que apuntara nuestra api, /api hace que se ejecute la funcion.
	//ruta http://localhost/asea/wordpress/wp-json ejemplo
	register_rest_route('contador','/api',array(
		'methods' => 'GET',
		'callback' => 'api_personalizada',
	));
});
//---------------------------------------------------------------
//referencias
//https://codex.wordpress.org/Class_Reference/wpdb
//https://codex.wordpress.org/Class_Reference/wpdb#INSERT_row
//https://codex.wordpress.org/Class_Reference/wpdb#UPDATE_rows
//https://codex.wordpress.org/Class_Reference/wpdb#SELECT_a_Variable

//https://codex.wordpress.org/AJAX_in_Plugins
//https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
?>
