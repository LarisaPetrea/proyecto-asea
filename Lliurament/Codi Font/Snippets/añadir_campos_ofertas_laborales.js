// Este snippet permite añadir nuevos campos para las ofertas laborales que se añaden por la parte de administrador.
// Documentación del plguin wp job manager: https://wpjobmanager.com/documentation/

// Añade el campo "Requisitos"
add_filter('job_manager_job_listing_data_fields', 'admin_add_requisitos_field' );

function admin_add_requisitos_field( $fields){
	$fields['_requisitos'] = array(
		'label'	 => __('Requisitos', 'job_manager'),
	  	'type' 	 => 'textarea',
	  	'description' => __('Cada requisito ha de estar separado por comas'),
	  	'priority'	=> 7
	);
  return $fields;
}

// Añade el campo "Contacto"
add_filter('job_manager_job_listing_data_fields', 'admin_add_contacto_field' );

function admin_add_contacto_field( $fields){
	$fields['_contacto_forma'] = array(
		'label'	 => __('Forma de contacto', 'job_manager'),
	  	'type'	 => 'text',
	  	'priority'	=> 4
	);
  return $fields;
}
// Añade el campo "responsabilidades"
add_filter('job_manager_job_listing_data_fields', 'admin_add_responsabilidades_field' );

function admin_add_responsabilidades_field( $fields){
	$fields['_responsabilidades'] = array(
		'label'	 => __('Responsabilidades', 'job_manager'),
	  	'type'	 => 'textarea',
	  	'description' => __('Cada resposabilidad ha de estar separada por comas'),
	  	'priority'	=> 5
	);
  return $fields;
}
// Añade el campo "otros"
add_filter('job_manager_job_listing_data_fields', 'admin_add_otros_field' );

function admin_add_otros_field( $fields){
	$fields['_otros'] = array(
		'label'	 => __('Otros', 'job_manager'),
	  	'type'	 => 'textarea',
	  	'description' => __('Han de estar separados por comas'),
	  	'priority'	=> 8
	);
  return $fields;
}

// Añade los nuevos campos en la parte frontend en la que se puede añadir un empleo, si llega a ser necesaria. 
//añade el campo "Requisitos" a la parte Frontend (formulario)
/*
add_filter('submit_job_form_fields', 'frontend_add_requisitos_field' );

function frontend_add_requisitos_field( $fields){
	$fields['job']['requisitos'] = array(
		'label'	 => __('Requisitos', 'job_manager'),
	  	'type' 	 => 'textarea',
	  	'required'    => true,
    	'priority'    => 7
	);
  return $fields;
}

//contacto
add_filter('submit_job_form_fields', 'frontend_add_contacto_field' );

function frontend_add_contacto_field( $fields){
	$fields['job']['contacto'] = array(
		'label'	 => __('Contacto', 'job_manager'),
	  	'type' 	 => 'text',
	  	'required'    => true,
    	'priority'    => 6
	);
  return $fields;
}

//responsabilidades
add_filter('submit_job_form_fields', 'frontend_add_responsabilidades_field' );

function frontend_add_responsabilidades_field( $fields){
	$fields['job']['responsabilidades'] = array(
		'label'	 => __('Responsabilidades', 'job_manager'),
	  	'type' 	 => 'textarea',
	  	'required'    => true,
    	'priority'    => 5
	);
  return $fields;
}

//otros
add_filter('submit_job_form_fields', 'frontend_add_otros_field' );

function frontend_add_otros_field( $fields){
	$fields['job']['otros'] = array(
		'label'	 => __('Otros', 'job_manager'),
	  	'type' 	 => 'textarea',
	  	'required'    => true,
    	'priority'    => 8
	);
  return $fields;
} */
