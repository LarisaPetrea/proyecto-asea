
// Este snippet añade una nueva zona para las zonas de widget, para poder añadir nuestro widget de empleos en la parte de la pagina que querramos

// Añadimos una nueva zona para widgets
add_action( 'widgets_init', 'dcms_agregar_nueva_zona_widgets' );

function dcms_agregar_nueva_zona_widgets() {
	register_sidebar( array(
		'name'          => 'Nueva Zona Widget',
		'id'            => 'idNuevaZona',
		'description'   => 'Descripción de la nueva Zona de Widgets',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
}

// Este snippet añade una caja con una bienvenida y un link al manual de usuario, en el escritorio de wordpress, así el usuario puede tenerlo a mano y poder acceder a el en cualquier momento y sin descargarlo. 

function manual_postbox_content() { 
  	echo '<p>Bienvenido a la página de administracion de ASEA, donde podrás añadir noticias, eventos y nuevas ofertas de empleo. No dudes en usar el manual de usuario para guiarte mejor.<p><ul><li><a href="">Manual de usuario</a>(temporal)</li></ul>';							   
} 
function manual_postbox_content_setup() { 
  $id = 'manual_postbox_desktop';
  $title = __( '¡Bienvenido!' ); 
  wp_add_dashboard_widget( $id, $title, 'manual_postbox_content' ); 
} 
add_action('wp_dashboard_setup', 'manual_postbox_content_setup');

