(function( $ ) {
	
	//Initializing jQuery UI Datepicker
	$( '#pae-event-start-date' ).datepicker({
		dateFormat: 'dd MM, yy',
		onClose: function( selectedDate ){
			$( '#pae-event-end-date' ).datepicker( 'option', 'minDate', selectedDate );
		}
	});
	$( '#pae-event-end-date' ).datepicker({
		dateFormat: 'dd MM, yy',
		onClose: function( selectedDate ){
			$( '#pae-event-start-date' ).datepicker( 'option', 'maxDate', selectedDate );
		}
	});

})( jQuery );