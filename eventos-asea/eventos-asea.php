<?php
/**
 * Plugin Name: Eventos ASEA
 * Description: Pluguin para crear eventos ASEA.
 * Version: 2.0
 * Author: Gabriel, Larisa, Cristina
 * License: GPL2
 */

/**
 * Indica las carpetas donde se guardan los respectivos recursos.
 */
define( 'ROOT', plugins_url( '', __FILE__ ) );
define( 'IMAGES', ROOT . '/img/' );
define( 'STYLES', ROOT . '/css/' );
define( 'SCRIPTS', ROOT . '/js/' );


/**
* Registra un post personalizado para los eventos.
* Funcion para crear el contenido de un evento.
* 'pae' (pluguin asea evento) es un dominio de texto estatico que sirve para traducir el contenido y que es unico para nuestro modulo.
*/
function pae_custom_post_type() {
	$labels = array(
  	'name'                  =>   __( 'Eventos', 'pae' ),
    'singular_name'         =>   __( 'Evento', 'pae' ),
    'add_new_item'          =>   __( 'Añadir nuevo evento', 'pae' ),
    'all_items'             =>   __( 'Todos los eventos', 'pae' ),
    'edit_item'             =>   __( 'Editar evento', 'pae' ),
    'new_item'              =>   __( 'Nuevo evento', 'pae' ),
    'view_item'             =>   __( 'Ver evento', 'pae' ),
    'not_found'             =>   __( 'No hay eventos', 'pae' ),
    'not_found_in_trash'    =>   __( 'No se ha encontrado ningun evento en la papelera', 'pae' )
    );

	/**
	 * El post tendrá un tiulo, el editor y el extracto.
	 *
	 */

	$supports = array(
		'title',
		'editor',
		'excerpt',
		'author'
	);

	$args = array(
		'label'				=>	__( 'Eventos', 'pae' ),
		'labels'			=>	$labels,
		'description'		=>	__( 'Lista de los proximos eventos', 'pae' ),
		'public'			=>	true,
		'show_ui'			=>	true,
		'show_in_menu'		=>	true,
		'show_in_rest'		=>	true,
		'show_in_admin_bar'	=>	true,
		'show_in_nav_menus'	=>	true,
		'menu_icon'			=>	IMAGES . 'event.svg',
		'has_archive'		=>	true,
		'rewrite'			=>	true,
		'pages'				=>	true,
		'supports'			=>	$supports,
		'taxonomies'		=>  array('category'),
		'has_archive'		=>	true
	);

	 /**
	  * Sirve para crear un tipo de contenido, que recibe dos parametros, el primero indica el tipo de contenido, y el segundo los argumentos.
	  * Esta funcion necesita ser llamada a traves del init. 
	  * Registra el evento.
	  */
	register_post_type( 'event', $args );
}
/*
 * Llama a la funcion pae_custom_post_type y ejecuta la funcion register_post_type a traves del init.
 */

add_action( 'init', 'pae_custom_post_type' );

/**
 * La siguiente funcion permite que las categorias sean buscadas, ya que al crear un post personalizado, wordpress no lo pilla
 * automaticamente. 
 */

function custom_posttype_query( $query ) {
    if( (is_category() || is_tag()) && $query->is_archive() && empty( 
        $query->query_vars['suppress_filters'] ) ) {
        $query->set( 'post_type', array(
           'post', 'event'
        ));
    }
    return $query;
}
add_filter( 'pre_get_posts', 'custom_posttype_query' );

/**
 * Limpia las reglas de escritura en la activacipn/desactivacion para un mejor funcionamiento de la estructura del enlace permanente
 */
function pae_activation_deactivation() {
	pae_custom_post_type();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'pae_activation_deactivation' );


/**
 * Añade campos extras en la creacion de eventos, en este caso, donde se nos permitira añadir la fecha de inicio, fecha final y la localizacion 
 */
function pae_add_event_info_metabox() {
	add_meta_box(
		'pae-event-info-metabox',
		__( 'Datos del evento', 'pae' ),
		'pae_render_event_info_metabox',
		'event',
		'side',
		'core'
	);
}
add_action( 'add_meta_boxes', 'pae_add_event_info_metabox' );


/**
 * Renderiza la metabox para la informacion de los eventos
 * @param $post es el objeto del post
 */
function pae_render_event_info_metabox( $post ) {
	// generamos campo nonce
	wp_nonce_field( basename( __FILE__ ), 'pae-event-info-nonce' );

	// obtiene los valores previamente guardados en los campos 
	$event_start_date = get_post_meta( $post->ID, 'event-start-date', true );
	$event_end_date = get_post_meta( $post->ID, 'event-end-date', true );
	$event_venue = get_post_meta( $post->ID, 'event-venue', true );

	// si no se ha guardado ninguna fecha, se pone la actual automaticamente
	$event_start_date = ! empty( $event_start_date ) ? $event_start_date : time();

	// si tampoco se ha guardado una fecha final, se asume que acaba el mismo dia
	$event_end_date = ! empty( $event_end_date ) ? $event_end_date : $event_start_date;

	?>
	<p> <!-- crea los inputs -->
		<label for="pae-event-start-date"><?php _e( 'Fecha de inicio:', 'pae' ); ?></label>
		<input type="text" id="pae-event-start-date" name="pae-event-start-date" class="widefat pae-event-date-input" value="<?php echo date( 'd F, Y', $event_start_date ); ?>" placeholder="Format: 28 Mayo, 2018">
	</p>
	<p>
		<label for="pae-event-end-date"><?php _e( 'Fecha final:', 'pae' ); ?></label>
		<input type="text" id="pae-event-end-date" name="pae-event-end-date" class="widefat pae-event-date-input" value="<?php echo date( 'd F, Y', $event_end_date ); ?>" placeholder="Format: 28 Mayo, 2018">
	</p>
	<p>
		<label for="pae-event-venue"><?php _e( 'Lugar del evento:', 'pae' ); ?></label>
		<input type="text" id="pae-event-venue" name="pae-event-venue" class="widefat" value="<?php echo $event_venue; ?>" placeholder="ej. Barcelona">
	</p>
	<?php
}


/*
 * Funcion que le indica al pluguin, donde encontrar el archivo de java script 'script.js' que esta dentro de la carpeta js, 
 * el archivo jquery viene incluida dentro de los archivos de wordpress, asi que  solo indicamos el nombre del archivo jquery. 
 * Indicamos el nombre del archivo css que esta dentro de la carpeta css.
 * Comprueba si la publicacion que esta siendo editada es de tipo evento.
 */
function pae_admin_script_style( $hook ) {
	global $post_type;

	if ( ( 'post.php' == $hook || 'post-new.php' == $hook ) && ( 'event' == $post_type ) ) {
		wp_enqueue_script(
			'upcoming-events',
			SCRIPTS . 'script.js',
			array( 'jquery', 'jquery-ui-datepicker' ),
			'1.0',
			true
		);

		wp_enqueue_style(
			'jquery-ui-calendar',
			STYLES . 'jquery-ui-1.10.4.custom.min.css',
			false,
			'1.10.4',
			'all'
		);
	}
}
add_action( 'admin_enqueue_scripts', 'pae_admin_script_style' );


/**
 * Guarda el evento en la base de datos
 * @param $post_id The id of the current post
 */
function pae_save_event_info( $post_id ) {
	// comprueba que el post es un evento
	if ( 'event' != $_POST['post_type'] ) {
		return;
	}

	//checking for the 'save' status
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST['pae-event-info-nonce'] ) && ( wp_verify_nonce( $_POST['pae-event-info-nonce'], basename( __FILE__ ) ) ) ) ? true : false;

	//exit depending on the save status or if the nonce is not valid
	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		return;
	}

	//checking for the values and performing necessary actions
	if ( isset( $_POST['pae-event-start-date'] ) ) {
		update_post_meta( $post_id, 'event-start-date', strtotime( $_POST['pae-event-start-date'] ) );
	}

	if ( isset( $_POST['pae-event-end-date'] ) ) {
		update_post_meta( $post_id, 'event-end-date', strtotime( $_POST['pae-event-end-date'] ) );
	}

	if ( isset( $_POST['pae-event-venue'] ) ) {
		update_post_meta( $post_id, 'event-venue', sanitize_text_field( $_POST['pae-event-venue'] ) );
	}
}
add_action( 'save_post', 'pae_save_event_info' );


/**
 * Custom columns head
 * @param  array $defaults The default columns in the post admin
 */
function pae_custom_columns_head( $defaults ) {
	unset( $defaults['date'] );

	$defaults['event_start_date'] = __( 'Fecha inicio', 'pae' );
	$defaults['event_end_date'] = __( 'Fecha final', 'pae' );
	$defaults['event_venue'] = __( 'Lugar', 'pae' );

	return $defaults;
}
add_filter( 'manage_edit-event_columns', 'pae_custom_columns_head', 10 );

/**
 * Custom columns content
 * @param  string 	$column_name The name of the current column
 * @param  int 		$post_id     The id of the current post
 */
function pae_custom_columns_content( $column_name, $post_id ) {
	if ( 'event_start_date' == $column_name ) {
		$start_date = get_post_meta( $post_id, 'event-start-date', true );
		echo date( 'd F, Y', $start_date );
	}

	if ( 'event_end_date' == $column_name ) {
		$end_date = get_post_meta( $post_id, 'event-end-date', true );
		echo date( 'd F, Y', $end_date );
	}

	if ( 'event_venue' == $column_name ) {
		$venue = get_post_meta( $post_id, 'event-venue', true );
		echo $venue;
	}
}
add_action( 'manage_event_posts_custom_column', 'pae_custom_columns_content', 10, 2 );

?>

<?php
/*
*Vistas de los eventos personalizados.
*
*/
/*
function vista_eventos(){
		$meta_quer_args = array(
			'relation'	=>	'AND',
			array(
				'key'		=>	'event-end-date',
				'value'		=>	time(),
				'compare'	=>	'>='
			)
		);

		$query_args = array(
			'post_type'				=>	'event',
			'tax_query'				=> array('relation' =>	'OR',
				array(
					'taxonomy'	=>	'category',
					'field'		=>	'evento-china',
					'terms'		=>	array( 'event' ),
					),
				array(
					'taxonomy'	=>	'category',
					'field'		=>	'evento-espana',
					'terms'		=>	array( 'event' ),
					),  
				array(
					'taxonomy'	=>	'category',
					'field'		=>	'eventos',
					'terms'		=>	array( 'event' ),
					),
				), 

			'posts_per_page'		=>	$instance['number_events'],
			'post_status'			=>	'publish',
			'ignore_sticky_posts'	=>	true,
			'meta_key'				=>	'event-start-date',
			'orderby'				=>	'meta_value_num',
			'order'					=>	'ASC',
			'meta_query'			=>	$meta_quer_args
		);

$upcoming_events = new WP_Query($query_args);

		//Preparing to show the events
		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		?>
		
		<ul class="pae_event_entries">
			<?php while( $upcoming_events->have_posts() ): $upcoming_events->the_post();
				$event_start_date = get_post_meta( get_the_ID(), 'event-start-date', true );
				$event_end_date = get_post_meta( get_the_ID(), 'event-end-date', true );
				$event_venue = get_post_meta( get_the_ID(), 'event-venue', true ); 
			?>
				<li class="pae_event_entry">
					<h4><a href="<?php the_permalink(); ?>" class="pae_event_title"><?php the_title(); ?></a> <span class="event_venue">at <?php echo $event_venue; ?></span></h4>
					<?php the_excerpt(); ?>
					<time class="pae_event_date"><?php echo date( 'd F, Y', $event_start_date ); ?> &ndash; <?php echo date( 'd F, Y', $event_end_date ); ?></time>
				</li>
			<?php endwhile; ?>
		</ul>

		<a href="<?php echo get_post_type_archive_link( 'event' ); ?>">View All Events</a>

		<?php
		wp_reset_query();

		//echo $after_widget;
	}
*/
//add_action( 'init', 'vista_eventos' );

?>

