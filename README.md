# README #

Este proyecto está creado por:

* Larisa Petrea
* Juan Gabriel Ramos
* Cristina García

### Descripción ###

Será una página web dedicada a los ALUMNI de ASEA, asociacion chino-española, y para la reintegración o la no perdida del contacto entre ellos.

Además, se pretende tener al alcance las diferentes tecnologías y empleos que puedan interesarles, como tambień una newsletter para todos lo que se suscriban.

### Utilización ###

Se prentende utilizar wordpress por la soltura y la gran variedad de applicaciones con la que poder controlar tanto las ofertas de trabajo, la seguridad dentro de la web  y la base de datos, además de poder tener un traductor in-web.



### Repositorios: ###

* Moqups: https://app.moqups.com/a16crigarrod@iam.cat/NBpTAckGBW/view
* Drive + documentos: https://drive.google.com/drive/folders/1EAp5K_LlQS2diOoL7DcGYNvKMvadqpl5
* Wrike o Asana : https://www.wrike.com/workspace.htm#path=mywork 
* Web de la Página : http://asea-asociacion.tk/
