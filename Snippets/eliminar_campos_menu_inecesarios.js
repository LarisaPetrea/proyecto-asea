// Este snippet elimina los campos que no son necesarios para el usuario, del menú de administración 

// Esconde "Proyectos" que viene predeterminado con el Divi, ya que no se necesita.
add_filter('et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1);
function mytheme_et_project_posttype_args($args) {
  return array_merge($args, array(
    'public'              => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'show_in_nav_menus'   => false,
        'show_ui'             => false
  ));
}
// Elimina "Comentarios" ya que tampoco se necesita gestionar comentarios. 
 function remove_admin_menus () { 
    global $menu; 
    $removed = array(  
        __('Comments')
        ); 
    end ($menu); 
    while (prev($menu)){ 
        $value = explode(
                ' ',
                $menu[key($menu)][0]); 
        if(in_array($value[0] != NULL?$value[0]:"" , $removed)){
            unset($menu[key($menu)]);
        }
    }  
 }
add_action('admin_menu', 'remove_admin_menus');