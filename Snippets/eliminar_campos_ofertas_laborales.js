// Este snippet permite eliminar los campos que no son necesarios para las ofertas laborales que se añaden por la parte de administrador.
// Documentación del plguin wp job manager: https://wpjobmanager.com/documentation/

// Elimina el campo "lema de la empresa" de la parte admin. 

add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_lema');
function admin_delete_field_lema( $fields ) {
    unset($fields['_company_tagline']);
    return $fields;
}
//	Elimina el campo "twitter" de la parte de admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_twitter');
function admin_delete_field_twitter( $fields ) {
    unset($fields['_company_twitter']);

    return $fields;
}
// Elimina el campo "video" de la parte de admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_video');
function admin_delete_field_video( $fields ) {
    unset($fields['_company_video']);

    return $fields;
}
//
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_job_expires');

function admin_delete_field_job_expires( $fields ) {
    unset($fields['_job_expires']);

    return $fields;
}
//
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_application');

function admin_delete_field_application( $fields ) {
    unset($fields['_application']);

    return $fields;
}
// Elimina el campo de "compañia"
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_company');

function admin_delete_field_company( $fields ) {
    unset($fields['_company_name']);

    return $fields;
}

//Elimina el campo "website de la compañia"
add_filter( 'job_manager_job_listing_data_fields', 'admin_delete_field_company_website');
function admin_delete_field_company_website( $fields ) {
    unset($fields['_company_website']);

    return $fields;
}


// Elimina los campos no necesarios en la parte frontend, si hiciera falta
/*
//elimina lema
add_filter( 'submit_job_form_fields', 'frontend_delete_field_lema');

function frontend_delete_field_lema( $fields ) {
    unset($fields['job']['_company_tagline']);

    return $fields;
}

//twitter
add_filter( 'submit_job_form_fields', 'frontend_delete_field_twitter');

function frontend_delete_field_twitter( $fields ) {
    unset($fields['job']['_company_twitter']);
  
    return $fields;
}
//video
add_filter( 'submit_job_form_fields', 'frontend_delete_field_video');

function frontend_delete_field_video( $fields ) {
    unset($fields['job']['_company_video']);
  
    return $fields;
}
//
add_filter( 'submit_job_form_fields', 'frontend_delete_field_job_expires');

function frontend_delete_field_job_expires( $fields ) {
    unset($fields['job']['_company_job_expires']);
  
    return $fields;
}
*/