// Este snippet muestra los nuevos campos añadidos para la parte admin, donde se muestran las ofertas de empleo. 

// Muestra el campo "requisitos"
add_action( 'single_job_listing_meta_after', 'display_job_requisitos' ); 

function display_job_requisitos(){
  global $post;
  
  $requisitos = get_post_meta( $post->ID, '_requisitos', true );

  if ( $requisitos ) { 
    echo // Crea un nuevo div donde se mostraran todos los campos que se añadan.
	  '<div class="todo" id="todo"> 
			<div class="requisitos_empleo">
				<span class="negrita">' . __( 'Requisitos' ) . '</span>
					<p>' . esc_html( $requisitos ) . '</p><br>
			</div>';
  }
}

// Muestra el campo "contacta con"
add_action( 'single_job_listing_meta_after', 'display_job_contacto_forma' ); 

function display_job_contacto_forma(){
  global $post;

  $contacto = get_post_meta( $post->ID, '_contacto_forma', true );

  if ( $contacto ) {
    echo 
	  	'<div class="contacto_forma">
	  		<span class="negrita">' . __( 'Contacta con' ) . '</span>
	  			<p>' . esc_html( $contacto ) . '</p><br>
		</div>';
  }
}
// Muestra el campo "responsabilidades"
add_action( 'single_job_listing_meta_after', 'display_job_responsabilidades' ); 

function display_job_responsabilidades(){
  global $post;

  $responsabilidades = get_post_meta( $post->ID, '_responsabilidades', true );

  if ( $responsabilidades ) {
    echo 
	  	'<div class="responsabilidades">
			<span class="negrita">' . __( 'Responsabilidades' ) . '</span>
	  			<p>'. esc_html( $responsabilidades ) . '</p><br>
		</div>';
  }
}
// Muestra el campo "otros"
add_action( 'single_job_listing_meta_after', 'display_job_otros' ); 

function display_job_otros(){
  global $post;

  $otros = get_post_meta( $post->ID, '_otros', true );

  if ( $otros ) {
    echo 
	  	'<div class="otros">
			<span class="negrita">' . __( 'Otros' ) . '</span> 
	  			<p>'. esc_html( $otros ) . '</p><br>
		</div>
	</div>';
  } // Aquí se cierra el div
}

// Muestra un título después de todos los campos anteriores, para que el campo predeterminado "descripción" tenga un título. 
add_action( 'single_job_listing_meta_after', 'display_job_title' );

function display_job_title(){
	echo ' <h6 class="negrita">Descripción</h6><br>';
  
  	
}
